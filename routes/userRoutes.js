const express = require("express");

const router = express.Router();


const auth  = require("../auth");
const {verify, verifyAdmin} = auth;






//Router for Registering User

const userControllers = require("../controllers/userControllers");
router.post("/",userControllers.registerUser);



//Router for LogIn
router.post("/login",userControllers.logInUser);

//Router for making admin
router.put("/setAdmin/:userId",verify,verifyAdmin,userControllers.setAdmin);

//Router for user details
router.get("/userDetails",verify,userControllers.userDetails);

//Router for user details
router.post("/addOrder",verify,userControllers.addOrder);


//Router for User Order
router.get("/getUserOrder",verify,userControllers.getUserOrder);

//Router for User Order
router.get("/getAllOrder",verify,verifyAdmin,userControllers.getAllOrder);





// check if email exists
router.post('/checkEmailExists',userControllers.checkEmailExists);
//Router for Display Products
// router.get("/myorder/:orderId",verify,userControllers.getMyOrder);

module.exports = router;